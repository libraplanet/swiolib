﻿using System;
using System.IO;
using System.Text;

namespace SwioLib.Common
{
    public class SwfHeaderMovie
    {
        public int MinX { get; private set; }
        public int MinY { get; private set; }
        public int MaxX { get; private set; }
        public int MaxY { get; private set; }
        public int FrameRateI { get; private set; }
        public int FrameRateF { get; private set; }
        public int FrameCount { get; private set; }

        public float FrameRate
        {
            get
            {
                return FrameRateI + (((float)FrameRateF) / 0xFF);
            }
        }
        

        class BitReader
        {
            public BinaryReader BaseReader { get; private set; }
            int length = 0;
            uint buffer = 0;
            public BitReader(BinaryReader stream)
            {
                BaseReader = stream;
            }

            public int getBit(uint n)
            {
                int ret = 0;
                for (int i = 0; i < n; i++)
                {
                    if (length == 0)
                    {
                        buffer = (uint)BaseReader.ReadByte();
                        length = 8;
                    }
                    ret <<= 1;
                    ret |= (int)(((buffer & 0xFF) >> 7) & 0x1);
                    buffer = (buffer << 1);
                    length -= 1;
                }
                return ret;
            }
        }

        public SwfHeaderMovie(BinaryReader reader)
        {
            BitReader r = new BitReader(reader);
            uint l = (uint)r.getBit(5);
            int minX = r.getBit(l);
            int minY = r.getBit(l);
            int maxX = r.getBit(l);
            int maxY = r.getBit(l);
            int frameRateF = reader.ReadByte();
            int frameRateI = reader.ReadByte();
            UInt16 frameCount = reader.ReadUInt16();
            this.MinX = minX;
            this.MinY = minY;
            this.MaxX = maxX;
            this.MaxY = maxY;
            this.FrameRateI = frameRateI;
            this.FrameRateF = frameRateF;
            this.FrameCount = frameCount;
        }
    }
}

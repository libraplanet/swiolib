﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.IO.Compression;

using SwioLib.IO;

namespace SwioLib.Common
{
    public class SwfObject
    {
        public SwfHeader SwfHeader { get; private set; }
        public SwfHeaderMovie SwfHeaderMovie { get; private set; }
        public SwTagBlockCollection SwTagBlockCollection { get; private set; }
        private static uint getByteSwapValue(uint i)
        {
            uint[] a = new uint[]{
                ((i >> 0)  & 0xFF),
                ((i >> 8)  & 0xFF),
                ((i >> 16) & 0xFF),
                ((i >> 24) & 0xFF),
            };
            return (a[0] << 24) | (a[1] << 16) | (a[2] << 8) | (a[3] << 0);
        }

        public static SwfObject Load(Stream stream)
        {
            using (BinaryReader bReader = new BinaryReader(stream, Encoding.ASCII))
            {
                SwfObject swf = new SwfObject();
                swf.SwfHeader = new SwfHeader(bReader);

                Action<BinaryReader> readBody = delegate(BinaryReader r)
                {
                    swf.SwfHeaderMovie = new SwfHeaderMovie(r);
                    swf.SwTagBlockCollection = new SwTagBlockCollection(r);
                };


                if (string.Equals(swf.SwfHeader.Magic, "CWS"))
                {
                    stream.ReadByte();
                    stream.ReadByte();
                    using (DeflateStream gStream = new DeflateStream(stream, CompressionMode.Decompress))
                    using (BinaryReader r = new BinaryReader(gStream, Encoding.ASCII))
                    {
                        readBody(r);
                    }

                }
                else
                {
                    readBody(bReader);
                }

                return swf;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace SwioLib.Common
{
    public class SwTagBlockCollection : List<SwTagBlock>
    {
        public SwTagBlockCollection(BinaryReader bReader) : base()
        {
            SwTagBlock tagBlock;
            do
            {
                tagBlock = new SwTagBlock(bReader);
                this.Add(tagBlock);
            }
            while (tagBlock.Tag != SwTagBlock.TagDefine.End);
        }
    }
}

﻿using System;
using System.Text;
using System.IO;
using System.IO.Compression;

namespace SwioLib.Common
{
    public class SwfHeader
    {
        public string Magic { get; private set; }
        public byte Version { get; set; }
        public uint FileSize { get; set; }

        public SwfHeader()
        {

        }

        public SwfHeader(BinaryReader bReader)
        {
            char[] magics = bReader.ReadChars(3);
            byte version = bReader.ReadByte();
            uint size = bReader.ReadUInt32();
            string magic = new string(magics);

            this.Magic = magic;
            this.Version = version;
            this.FileSize = size;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using Shapes = System.Windows.Shapes;
using Microsoft.Win32;

using System.IO;
using System.IO.Compression;
using System.Diagnostics;

using SwioLib.Common;

namespace SampleWindow
{
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
#if DEBUG
            Uri uri = new Uri(Path.GetFullPath(@"../../..//sample/SampleSwf00.swf"));
            textBoxPath.Text = uri.LocalPath;
#endif
        }
        #region[methods]
        private void fixStateStyle()
        {
            if (this.WindowState == System.Windows.WindowState.Maximized)
            {
                BorderThickness = new Thickness(6);
                this.buttonNormalize.Visibility = System.Windows.Visibility.Visible;
                this.buttonMaximize.Visibility = System.Windows.Visibility.Collapsed;
            }
            else
            {
                BorderThickness = new Thickness(1);
                this.buttonNormalize.Visibility = System.Windows.Visibility.Collapsed;
                this.buttonMaximize.Visibility = System.Windows.Visibility.Visible;
            }
        }
        private void message(string s)
        {
            const int MAX_LINE = 100;
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(string.Format("[{0}] {1}", DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss.fff"), s));
            using (StringReader r = new StringReader(textBoxMessage.Text))
            {
                for (int i = 0; i < MAX_LINE - 1; i++)
                {
                    sb.AppendLine(r.ReadLine());
                }
            }
            textBoxMessage.Text = sb.ToString();
        }

        private void selectFile()
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "all file|*|swf|*.swf;*.swf";
            dialog.FilterIndex = 2;
            if (dialog.ShowDialog() == true)
            {
                string path = dialog.FileName;
                textBoxPath.Text = null;
                textBoxPath.Text = path;
                message(string.Format("[MainWindow.selectFile()] selected({0})", new object[] { path }));
            }
        }

        #endregion

        #region [event]
        private void window_Initialized(object sender, EventArgs e)
        {
            fixStateStyle();
        }
        private void window_StateChanged(object sender, EventArgs e)
        {
            fixStateStyle();
        }
        #endregion

        #region [command]
        private void CommandCloseCommand(object sender, RoutedEventArgs e)
        {
            SystemCommands.CloseWindow(this);
        }
        private void CommandMaximizeWindow(object sender, RoutedEventArgs e)
        {
            SystemCommands.MaximizeWindow(this);
        }
        private void CommandMinimizeWindow(object sender, RoutedEventArgs e)
        {
            SystemCommands.MinimizeWindow(this);
        }
        private void CommandRestoreWindow(object sender, RoutedEventArgs e)
        {
            SystemCommands.RestoreWindow(this);
        }
        #endregion

        #region [menu]
        private void FileMenuOpen(object sender, RoutedEventArgs e)
        {
            selectFile();
        }
        private void WindowMenuTopMost(object sender, RoutedEventArgs e)
        {
            this.Topmost = !this.Topmost;
            menuItemTopMost.IsChecked = this.Topmost;
        }
        private void HelpMenuVersion(object sender, RoutedEventArgs e)
        {
        }
        #endregion

        #region [event2]
        private void textBoxPath_TextChanged(object sender, TextChangedEventArgs e)
        {
            string path = textBoxPath.Text;
            if (path != null)
            {
                try
                {
                    message(string.Format("[MainWindow.openFile()] load start({0})", new object[] { path }));
                    SwfObject swfObj = null;
                    using (FileStream stream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                    {
                        List<string> list = new List<string>();
                        swfObj = SwfObject.Load(stream);
                        list.Add(string.Format("  SwfObject.SwHeader.Magic            = {0};", new object[] { swfObj.SwfHeader.Magic }));
                        list.Add(string.Format("  SwfObject.SwHeader.Version          = {0};", new object[] { swfObj.SwfHeader.Version }));
                        list.Add(string.Format("  SwfObject.SwHeader.FileSize         = {0:#,0};", new object[] { swfObj.SwfHeader.FileSize }));
                        list.Add(string.Format("  SwfObject.SwHeader.FileSize         = {0:X};", new object[] { swfObj.SwfHeader.FileSize }));
                        list.Add(string.Format("  SwfObject.SwfHeaderMovie.X          = {0}twips ({1}pt);", new object[] { swfObj.SwfHeaderMovie.MinX, swfObj.SwfHeaderMovie.MinX / 20 }));
                        list.Add(string.Format("  SwfObject.SwfHeaderMovie.Y          = {0}twips ({1}pt);", new object[] { swfObj.SwfHeaderMovie.MinY, swfObj.SwfHeaderMovie.MinY / 20 }));
                        list.Add(string.Format("  SwfObject.SwfHeaderMovie.Width      = {0}twips ({1}pt);", new object[] { swfObj.SwfHeaderMovie.MaxX, swfObj.SwfHeaderMovie.MaxX / 20 }));
                        list.Add(string.Format("  SwfObject.SwfHeaderMovie.Height     = {0}twips ({1}pt);", new object[] { swfObj.SwfHeaderMovie.MaxY, swfObj.SwfHeaderMovie.MaxY / 20 }));
                        list.Add(string.Format("  SwfObject.SwfHeaderMovie.FrameRate  = {0:0.0});", new object[] { swfObj.SwfHeaderMovie.FrameRate }));
                        list.Add(string.Format("  SwfObject.SwfHeaderMovie.FrameCount = {0});", new object[] { swfObj.SwfHeaderMovie.FrameCount }));
                        for(int i = 0; i < swfObj.SwTagBlockCollection.Count; i++)
                        {
                            SwTagBlock tagBlock = swfObj.SwTagBlockCollection[i];
                            list.Add(string.Format("  SwfObject.SwTagBlockCollection[{0}].Tag         = {1});", new object[] { i, tagBlock.Tag }));
                            list.Add(string.Format("  SwfObject.SwTagBlockCollection[{0}].Length      = {1});", new object[] { i, tagBlock.Length }));
                            list.Add(string.Format("  SwfObject.SwTagBlockCollection[{0}].ShortLength = {1});", new object[] { i, tagBlock.ShortLength }));
                            list.Add(string.Format("  SwfObject.SwTagBlockCollection[{0}].LongLength  = {1});", new object[] { i, tagBlock.LongLength }));
                        }
                        list.Reverse();
                        foreach(string s in list)
                        {
                            message(s);
                        }
                        message(string.Format("[MainWindow.openFile()] loaded."));
                    }
                    foreach (SwTagBlock tagBlock in swfObj.SwTagBlockCollection)
                    {
                        if (tagBlock.Tag == SwTagBlock.TagDefine.DefineSound)
                        {
                            message(string.Format("[MainWindow.openFile()] sound find!."));
                            string tmpFileName = Path.GetTempFileName() + ".mp3";
                            using (FileStream stream = new FileStream(tmpFileName, FileMode.Create, FileAccess.Write, FileShare.ReadWrite))
                            {
                                stream.Write(tagBlock.Data, 0, tagBlock.Data.Length);
                            }
                            mediaElement.Source = new Uri(tmpFileName, UriKind.Absolute);
                            mediaElement.Play();
                            break;
                        }
                    }
                }
                catch (Exception ex)
                {
                    message(string.Format("[MainWindow.openFile()] {0}", new object[] { ex.ToString() }));
                    message(string.Format("[MainWindow.openFile()] faild...({0})", new object[] { path }));
                }
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            selectFile();
        }
        private void mouseDragEnter(object sender, DragEventArgs e)
        {
            e.Effects = DragDropEffects.Copy;
        }

        private void mouseDrop(object sender, DragEventArgs e)
        {
            try
            {
                string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
                string file = files[0];
                textBoxPath.Text = null;
                textBoxPath.Text = file;
                message(string.Format("[MainWindow.textBoxPath_Drop()] succeed"));
            }
            catch (Exception ex)
            {
                message(string.Format("[MainWindow.textBoxPath_Drop()] failed... {0}", new object[] { ex.ToString() }));
            }
        }

        private void ButtonPlay(object sender, RoutedEventArgs e)
        {
            try
            {
                mediaElement.Stop();
                mediaElement.Play();
                message(string.Format("[MainWindow.ButtonPlay()] succeed"));
            }
            catch (Exception ex)
            {
                message(string.Format("[MainWindow.ButtonPlay()] failed... {0}", new object[] { ex.ToString() }));
            }
        }

        private void ButtonStop(object sender, RoutedEventArgs e)
        {
            try
            {
                mediaElement.Stop();
                message(string.Format("[MainWindow.ButtonStop()] succeed"));
            }
            catch (Exception ex)
            {
                message(string.Format("[MainWindow.ButtonStop()] failed... {0}", new object[] { ex.ToString() }));
            }
        }
        private void mediaElement_MediaOpened(object sender, RoutedEventArgs e)
        {
            mediaElement.Play();
        }

        private void mediaElement_MediaEnded(object sender, RoutedEventArgs e)
        {
            TimeSpan time = new TimeSpan(0, 0, 0, 0, 0);
            mediaElement.Position = time;
        }
        #endregion

    }
}

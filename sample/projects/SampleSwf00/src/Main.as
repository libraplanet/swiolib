package
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.media.Sound;
	
	/**
	 * ...
	 * @author takumi
	 */
	public class Main extends Sprite 
	{
		[Embed(source="../res/sample_00.mp3", mimeType="audio/mpeg")]
		public static const SND_00:Class;
		
		[Embed(source="../res/sample_01.mp3", mimeType="audio/mpeg")]
		public static const SND_01:Class;
		
		public function Main() 
		{
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			// entry point
			
			
		}
		
	}
	
}